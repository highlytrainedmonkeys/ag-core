import codecs
import logging
import os

from django.apps import AppConfig
from django.db.models.signals import post_migrate


logger = logging.getLogger(__name__)

ENTITY_TYPES = {
    "p": "Person",
    "c": "Company",
}


def import_country_codes(sender, **kwargs):
    EU_MEMBERS = [
        "AT",
        "BE",
        "HR",
        "BG",
        "CY",
        "CZ",
        "DK",
        "EE",
        "FI",
        "FR",
        "DE",
        "GR",
        "HU",
        "IE",
        "IT",
        "LV",
        "LT",
        "LU",
        "MT",
        "NL",
        "PL",
        "PT",
        "RO",
        "SK",
        "SI",
        "ES",
        "SE",
    ]
    iso_countries_file = os.path.dirname(__file__) + os.path.sep + "assets/iso_countries.txt"

    try:
        f = codecs.open(iso_countries_file, "r", "utf-8")
        for line in f.readlines():
            line = line.strip()
            (cname, cc) = line.split(";")
            is_eu_member = True if cc in EU_MEMBERS else False
            c_model = sender.get_model("CountryCode")
            country = c_model(name=cname, code=cc, is_eu_member=is_eu_member)
            country.save()
    except Exception as err:
        logger.error("Can't import country codes from file: %s", format(err))
        return


def import_entity_types(sender, **kwargs):
    for code, name in ENTITY_TYPES.items():
        e_model = sender.get_model("EntityType")
        instance = e_model(code=code, name=name)
        instance.save()


class Config(AppConfig):
    name = "ag_core"
    verbose_name = "AG Core"

    def ready(self):
        post_migrate.connect(import_country_codes, sender=self)
        post_migrate.connect(import_entity_types, sender=self)
