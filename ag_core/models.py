from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from phonenumber_field.modelfields import PhoneNumberField


class CommonModel(TimeStampedModel):
    class Meta:
        abstract = True


class CommentModel(CommonModel):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    text = models.TextField()

    class Meta:
        abstract = True


class CodeNameModel(models.Model):
    code = models.CharField(
        _("Code"),
        max_length=16,
        primary_key=True,
    )
    name = models.CharField(
        _("Name"),
        max_length=128,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class EntityType(CodeNameModel):
    """
    Entity type model (private person, company, etc.)
    """

    class Meta:
        db_table = "entity_type"
        verbose_name = _("Entity Type")


class CountryCode(CodeNameModel):
    code = models.CharField(
        max_length=2,
        primary_key=True,
    )
    is_eu_member = models.BooleanField(
        db_index=True,
        default=True,
        verbose_name=_("EU Member"),
    )

    class Meta:
        db_table = "country_code"
        verbose_name = _("Country Code")
        verbose_name_plural = _("Country Codes")

    def __str__(self):
        return self.name


class AddressModel(CommonModel):
    type = models.ForeignKey(
        EntityType,
        on_delete=models.PROTECT,
        verbose_name=_("Type"),
    )
    name = models.CharField(
        _("Name"),
        max_length=128,
        help_text=_(
            "Person or Company Name",
        ),
    )
    id1 = models.CharField(
        _("CNP/Fiscal Code"),
        max_length=128,
        help_text=_("The VAT number for companies, CNP for persons."),
    )
    id2 = models.CharField(
        _("Registration Number"),
        max_length=128,
        null=True,
        blank=True,
        help_text=_("""Mandatory for Romanian companies."""),
    )
    email = models.EmailField(
        _("Email"),
        max_length=254,
    )
    country_code = models.ForeignKey(
        CountryCode,
        on_delete=models.PROTECT,
        verbose_name=_("Country"),
        default="RO",
    )
    city = models.CharField(
        _("City"),
        max_length=64,
    )
    postal_code = models.CharField(
        _("Postal Code"),
        max_length=64,
        null=True,
        blank=True,
    )
    street1 = models.CharField(
        _("Address Line #1"),
        max_length=256,
    )
    street2 = models.CharField(
        _("Address Line #2"),
        max_length=256,
        blank=True,
        null=True,
    )
    street3 = models.CharField(
        _("Address Line #3"),
        max_length=256,
        blank=True,
        null=True,
    )
    phone = PhoneNumberField(
        _("Phone"),
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True

    def __str__(self):
        snip = "%s, %s " % (self.email, self.street1)

        if self.street2 not in [None, ""]:
            snip = snip + ", %s" % self.street2

        if self.street3 not in [None, ""]:
            snip = snip + ", %s" % self.street3

        return snip + ", %s, %s" % (self.city, self.country_code)


class BankDetails(models.Model):
    bank_name = models.CharField(
        _("Bank Name"),
        max_length=128,
        null=True,
        blank=True,
    )
    bank_account = models.CharField(
        _("Bank Account"),
        max_length=128,
        null=True,
        blank=True,
    )

    class Meta:
        abstract = True
