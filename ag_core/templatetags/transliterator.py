from django import template
from django.template.defaultfilters import stringfilter

from isounidecode import unidecode

register = template.Library()


@register.filter(name="transliterate")
@stringfilter
def transliterate(value: str) -> str:
    clean = unidecode(value)
    return clean.decode("utf-8")
