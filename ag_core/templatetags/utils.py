from django import template

register = template.Library()


@register.filter(name="field_type")
def field_type(field: any) -> str:
    return field.field.widget.__class__.__name__


@register.filter(name="value_type")
def value_type(value: any) -> str:
    return type(value).__name__
