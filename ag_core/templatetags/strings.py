from django import template

register = template.Library()


@register.filter
def split(value: str, arg: str) -> list:
    return list(filter(None, value.split(arg)))


@register.filter
def deslugify(value: str) -> str:
    return value.replace("-", " ").replace("_", " ")
