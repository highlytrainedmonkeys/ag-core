import pytest

from ag_core.models import CountryCode, EntityType


@pytest.mark.django_db
def test_entity_types_setup():
    assert EntityType.objects.filter(code="p").exists()
    assert EntityType.objects.filter(code="c").exists()


@pytest.mark.django_db
def test_country_codes_setup():
    assert CountryCode.objects.filter(code="RO").exists()

    eu = CountryCode.objects.get(code="FR")

    assert eu.is_eu_member
