DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3"}}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

SECRET_KEY = "secrekey"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "phonenumber_field",
    "ag_core",
    "tests",
]
