import pytest

from ag_core.templatetags.strings import split, deslugify
from ag_core.templatetags.transliterator import transliterate
from ag_core.templatetags.utils import value_type


@pytest.mark.django_db
def test_split():
    res = split("a,b", ",")

    assert isinstance(res, list)
    assert "a" in res
    assert "b" in res


@pytest.mark.django_db
def test_deslugify():
    res = deslugify("a-b")

    assert "a b" == res


@pytest.mark.django_db
def test_transliterate():
    res = transliterate("ștanță")

    assert "stanta" == res


@pytest.mark.django_db
def test_value_type():
    a = 2
    res = value_type(a)

    assert res == "int"


# TODO add a test for this tag
# @pytest.mark.django_db
# def test_field_type():
#     field = CharField()

#     res = field_type(field)

#     assert res == "CharField"
