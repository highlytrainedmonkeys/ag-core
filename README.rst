=======
AG Core
=======

AG Core is a collection of core models and utils that are being used by AgileGeeks Django modules (accounting for the moment).

Quick start
-----------

1. Add "ag_core" to your INSTALLED_APPS settings like this::

    INSTALLED_APPS = [
        'django.contrib.staticfiles',
        'ag_core',
    ]

2. Profit