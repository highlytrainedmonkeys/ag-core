#!/bin/sh
set -e

echo "Initialization done! You may connect to the container."

exec "$@"