FROM python:3.9-alpine

RUN apk update; apk add gcc alpine-sdk

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /home/src

COPY requirements-dev.txt .
COPY requirements.txt .

RUN pip install --upgrade pip && pip install -r requirements-dev.txt

COPY . .